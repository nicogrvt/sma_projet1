from mesa import Agent
from prey_predator.random_walk import RandomWalker


class Sheep(RandomWalker):
    """
    A sheep that walks around, reproduces (asexually) and gets eaten.

    The init is the same as the RandomWalker.
    """

    def __init__(self, unique_id, pos, model, moore, energy, max_energy, lifespan):
        super().__init__(unique_id, pos, model, moore=moore)
        self.initial_energy = energy
        self.energy = energy
        self.max_energy = max_energy
        self.lifespan = lifespan
        self.stepstodeath = lifespan

    def step(self):
        """
        A model step. Move, then eat grass and reproduce.
        """
        self.random_move()

        if self.model.grass:

            # Reduce energy
            self.energy -= 1
            self.stepstodeath -= 1

            # If there is grass available, eat it

            current_cell = self.model.grid.get_cell_list_contents([self.pos])
            grass_patch = [x for x in current_cell if isinstance(x, GrassPatch)][0]

            if grass_patch.fully_grown and self.energy <= 2*self.initial_energy:
                self.energy += self.model.sheep_gain_from_food
                grass_patch.fully_grown = False

        if self.energy > 0 and self.random.random() < self.model.sheep_reproduce and self.stepstodeath >0:
            # Create a new sheep:

            if self.model.grass:
                self.energy = self.energy/2

            lamb = Sheep(self.model.next_id(),
                         self.pos,
                         self.model,
                         self.moore,
                         self.energy,
                         self.max_energy,
                         self.lifespan
                )

            self.model.grid.place_agent(lamb, self.pos)
            self.model.schedule.add(lamb)


        # Death
        if self.energy <= 0 or self.stepstodeath <=0:
            self.model.grid._remove_agent(self.pos, self)
            self.model.schedule.remove(self)


class Wolf(RandomWalker):
    """
    A wolf that walks around, reproduces (asexually) and eats sheep.
    """

    def __init__(self, unique_id, pos, model, moore, energy, max_energy, lifespan):
        super().__init__(unique_id, pos, model, moore=moore)
        self.energy = energy
        self.lifespan = lifespan
        self.stepstodeath = lifespan
        self.initial_energy = energy
        self.max_energy = max_energy

    def step(self):
        self.random_move()

        # at each step remove energy
        self.energy -= 1
        self.stepstodeath -= 1

        # verify if sheep in the cell
        current_cell = self.model.grid.get_cell_list_contents([self.pos])
        sheep = [x for x in current_cell if isinstance(x, Sheep)]

        # If sheep in the cell, eat it
        if len(sheep) > 0 and self.energy <= 2* self.initial_energy:
            sheep_to_eat = self.random.choice(sheep)
            self.energy += self.model.wolf_gain_from_food

            # Kill the sheep
            self.model.grid._remove_agent(self.pos, sheep_to_eat)
            self.model.schedule.remove(sheep_to_eat)


        if self.energy > 0 and self.random.random() < self.model.wolf_reproduce and self.stepstodeath > 0:

            # Create a new wolf cub. Possibility of death
            self.energy = self.energy / 2

            cub = Wolf(self.model.next_id(),
                       self.pos,
                       self.model,
                       self.moore,
                       self.energy,
                       self.max_energy,
                       self.lifespan
                       )

            self.model.grid.place_agent(cub, cub.pos)
            self.model.schedule.add(cub)

        # Death or reproduction
        if self.energy <= 0 or self.stepstodeath <= 0:
            self.model.grid._remove_agent(self.pos, self)
            self.model.schedule.remove(self)



class GrassPatch(Agent):
    """
    A patch of grass that grows at a fixed rate and it is eaten by sheep
    """

    def __init__(self, unique_id, pos, model, fully_grown, countdown):
        """
        Creates a new patch of grass

        Args:
            grown: (boolean) Whether the patch of grass is fully grown or not
            countdown: Time for the patch of grass to be fully grown again
        """
        super().__init__(unique_id, model)
        self.fully_grown = fully_grown
        self.countdown = countdown
        self.pos = pos

    def step(self):

        # if fully grown, then set the countdown back
        # if not discount 1 from it

        if not self.fully_grown:
            if self.countdown <= 0:
                # Set as fully grown
                self.fully_grown = True
                self.countdown = self.model.grass_regrowth_time
            else:
                self.countdown -= 1        

# SMA_Projet1

Prey vs predator model : wolf-sheep predation with NetLogo (http://ccl.northwestern.edu/netlogo/models/WolfSheepPredation)

## Problem to Solve

In the prey vs predator model our objective is to find a stable version of what occurs in nature between the aformentioned 2 groups. This means that (1) we should obtain some type of cyclical response i.e. the lesser the amount of preys, the lesser the amount of predators as food becomes scarces, this, in turn, contributes to an increase in the number of preys as the risk of being eaten decreased which in turn leads to an increase in the number of predators etc. A graph summarizing this point can be found below. (2) Our model has to make some simplifications as there are too many interactions to take into account, hence we need to carefully choose the variables that should be modified to reach such a result.


![Alt text](images/Predator_prey_curve.png?raw=true "Title")


## Hypothesis

Our hypothesis to reach this. cyclical stability are the following:

(1) Not Eating will lead to a loss of energy and therefore death
(2) Living beings have a lifespan irrespective of their level of energy: i.e. if they are full of energy but pass their lifespan then they should die
(3) We initialize as if we came as an observer in the environment i.e. not all predators/preys are hungry from the get-go 
(4) As occurs in nature, giving birth uses energy and is a relatively dangeroous time for both the mother and child i.e. higher rate of death

If these assumptions are met, we hypothesis that if the absolute number of both species is within a respectable threshold at intialization, and there are no big discrepancies in the birth rate and energy that food provides, then a cycle with low risk of collapsing will form.


## Our take on Prey vs predator

As mentioned in our hypothesis, we had many assumptions that translated in our choice of variables for the sifferent agents. Both the prey and predator could perish in various manners: (1) Dying of old age, this is equivalent to our variable "lifespan" which represents the maximum steps an agent can live for. It is decreased by one at every step and once it reaches 0 the agent is discarded. (2) Dying from hunger / loss of energy, at each step their energy is decreased by one and divided by two if the agent gives birth, some energy canbe regained by eating meat / grass for wolves /sheep. (3) Dying from giving birth, a very low probability is affected to the agents in order to represent this risky period for both the newborn and the mother.

A number of parameters are modifiable through the Graphical User Interface (GUI) and include the grass regrowth time, the lifespan, the initial population, the reproduction rate and the energy gained from food. These also inform other variables which are implicitly affected by changes. For instance, changing the energy gained from food will impact the maximum energy the agent can have which is set at twice the latter, furthermore, the initial energy of each animal is set at random between the energy gained from food and 1.5 times the maximum energy. To note that by maximum energy we actually mean the energy above which eating is not considered.

Initializing the agents with random values for the energy is important as it avoids cases where all of the wolves would be hungry and hence would eat the sheep directly which would force us to articially increase the initial number of sheep, or increase their birth reate in order to attempt to have some cyclical pattern.


## Results

Thanks to a proper initialization of our agents and with some tweaking of the parameters we find a set of solutions for our population density to be cyclical. Without having a random initiialization of the energy and a maximum energy, we wouldn't be able to initialize the number of wolves to be equal to the number of sheep as they would all be seeking to eat and hence would end up eating all of them. Hence, the initial ratio of sheep to wolf is not important, to a certain degree, (obviously if we have 3 times the number of wolves, there is a much higher probability of having th wolves eat all of the sheep). The absolute number also tends to have an importance as the map is limited in size, if we initialize with more than 300 sheeps, the wolves would always find a sheep to eat and reproduce, grow to over 300 and cover almost the entirety of the map making almost certain for the wolves to eat all of the sheep. Finally, the reproduction rate is key, if we set it too high we might encounter the exact same issue as mentioned above.

#### 100 Sheep
![Alt text](images/100_sheep.png?raw=true "Sheep 200")

#### 200 Sheep
![Alt text](images/200_sheep.png?raw=true "Sheep 200")

#### 300 Sheep
![Alt text](images/300_sheep.png?raw=true "Sheep 300")


Although these 3 graphs present cycles, the issue is that setting to high of a number and such a difference in reproduction rates creates some important variability e.g. if we look at the lower values, they are often very close to 0 which means there is a huge risk for the whole ecosystem to collapse. In this regard, here is one system that could be considered as optimal. And does prove our initial hypothesis that within a threshold in this case we chose 50 wolves and 50 sheep, and a low discrepancy between the reproduction rates and energy received from food, a cycle with low risk of collapse would form.


#### Optimal combination
![Alt text](images/optimal_combination.png?raw=true "Sheep 300")


| Name | # Init Sheep  | # Init Wolves | grass growth % | lifespan | wolf reproduction % | sheep reproduction % | wolf food energy | sheep food energy |
|------|------|------|------|------|------|------|------|------|
|   Sheep 300  | 300 | 20 | 1/8 | 100 | 5% | 20% | 20 | 5|
|   Sheep 200  | 200 | 20 | 1/8 | 100 | 5% | 20% | 20 | 5|
|   Sheep 100  | 200 | 20 | 1/8 | 100 | 5% | 20% | 20 | 5|
|   Optimal combination  | 50 | 50 | 1/8 | 100 | 20% | 20% | 10 | 5|




